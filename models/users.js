const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
	fullname: {
		type: String,
		required: [true, "Fullname is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
});



module.exports = mongoose.model("Users", userSchema)