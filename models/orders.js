const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
	buyer: [
		{
			fullname: {
				type: String,
				required: [true, "Buyer fullname is required"]
			},
			userId: {
				type: String,
				required: [true, "Buyer ID is required"]
			},
			userEmail: {
				type: String,
				required: [true, "Buyer email is required"]
			}
		}
	],
	product: [
		{
			name: {
				type: String,
				required: [true, "Product name is required"]
			},
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Product quantity is required"]
			},
			price: {
				type: Number,
				required: [true, "Product price is required"]
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, "Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Orders", orderSchema)