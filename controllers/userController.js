const Users = require('../models/users');
const Orders = require('../models/orders');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Post functions
// Registration w/ email validation
module.exports.registration = (request => {
	return Users.find({email: request.email}).then(result => {
		if(result.length > 0){
			return false
		} else {
			let newUser = new Users({
				fullname: request.fullname,
				email: request.email,
				password: bcrypt.hashSync(request.password, 10)
			})

			return newUser.save()
			.then(registerUser => {return registerUser})
			.catch(error => {return error})
		}
	})
});

// Post User Token
module.exports.getToken = (request) => {
	return Users.findOne({email: request.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(request.password, result.password)
			if(isPasswordCorrect) {
				return ({Token: auth.createToken(result)})
			} else {
				return false
			}
		}
	})
};

// Check if admin
module.exports.checkUser = async (data) => {

	return Users.findById(data.id)
	.then(result => {

		if (result == null) {
			return false
		} else {
			console.log(result)
			// result.password = "***"
			return result
		}
	}).catch(error => {return error})
};

// Retrieve User Details
module.exports.userDetails = async (user) => {
	if(user.isAdmin == true){
		return Users.find({})
		.then(result => {return result})
		.catch(error => {return error})
	} else {
		return Users.findById(user.id)
		.select({'_id': 0})
		.then(result => {return result})
		.catch(error => {return error})
	}
};

// Retrieve Order Details
module.exports.userOrderDetails = async (user) => {
	return Orders.find({'buyer.userId': user.id})
		.select({'buyer': 0})
		.then(result => {return result})
		.catch(error => {return error})

	// return orderDetails

	if(result){
		return true
	}else {
		return false
	}
};

module.exports.orderDetails = async (user) => {
	return Orders.find({})
	.then(result => {return result})
	.catch(error => {return error})

	// return orderDetails

	if(result){
		return true
	}else {
		return false
	}
};

// Set user as admin
module.exports.setUser = async (request, admin) => {
	if(admin.isAdmin == true){
		let setAdmin = {
			isAdmin: request.isAdmin
		}

		return Users.findByIdAndUpdate(request.id, setAdmin)
		.then(result => {return result})
		.catch(error => {return error})
	} else {
		return ("You are not allowed to change status")
	}
};