const Products = require('../models/products');
const Users = require('../models/users');
const auth = require('../auth');

// Post functions
// Add Products
module.exports.addProduct = async (request, checkAdmin) => {
	if (checkAdmin == false) {
		return false
	} else {
		let newProducts = new Products ({
				name: request.name,
				description: request.description,
				price: request.price,
				quantity: request.quantity
			})
		return newProducts.save()
		.then(result => {return result})
		.catch(error => {return error})		
		}
};
// Retrieve All Products
module.exports.allProducts = () => {
	return Products.find({})
	.then(result => {return result})
	.catch(error => {return error})
};

// Retrieve Active Products
module.exports.activeProducts = () => {
	return Products.find({isActive: true})
	.then(result => {return result})
	.catch(error => {return error})
};

module.exports.inactiveProducts = () => {
	return Products.find({isActive: false})
	.then(result => {return result})
	.catch(error => {return error})
};

// Retrieve certain product
module.exports.isProduct = (reqParams) => {
	return Products.findById(reqParams.productId)
	.then(result => {return result})
	.catch(error => {return error})	
	};

// Update product description
module.exports.updateProduct = async (request, checkAdmin) => {
	let updatedProduct = {
		name: request.name,
		description: request.description,
		price: request.price,
		quantity: request.quantity,
		isActive: request.isActive
	}
	return Products.findByIdAndUpdate(request.id, updatedProduct).then(result => {return result})
		.catch(error => {return error})
		
	if (error){
		return false
	} else {
		return true
	}
};

// Archive Product
module.exports.updateStatus = async (request, checkAdmin) => {

		return Products.findByIdAndUpdate(request.id)
		.then(result => {return result})
		.catch(error => {return error})
		
	if (error){
		return false
	} else {
		return true
	}
};