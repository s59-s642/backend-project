// Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Modules
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')

// Connection
const app = express();
const port = process.env.PORT || 5000;

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// mongoDB Connection
mongoose.connect(`mongodb://lrndlacrz:admin123@ac-mvlgpdf-shard-00-00.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-01.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-02.c2nhgts.mongodb.net:27017/Capstone3?ssl=true&replicaSet=atlas-ika0ge-shard-0&authSource=admin&retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Main URI
app.use("/users", userRoutes)
app.use("/products", productRoutes)
app.use("/orders", orderRoutes)

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection'))
db.once('open', () => console.log('Connected to Server!'))

app.listen(port, () => console.log(`API is running at ${port}`))