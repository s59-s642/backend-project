const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController')
const auth = require('../auth')

// Routes
router.post("/addOrders", auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization)

	orderController.createOrders(request.body, user).then(resultFromController => response.send(resultFromController))
})

module.exports = router;